const Path = require('path');

const Utils = require('./Utils.js').Utils;

class IModule
{
        constructor(name,commandName)
        {
                this.name = name;
                this.command = commandName;

        }      

        /**
         * Loads the module's settings
         * @param {Client} bot The Discord bot 
         */
        LoadModuleSettings(bot)
        {
                if(typeof bot.settings.misc[this.name] === 'undefined')
                {
                        this.Settings = {};      
                }
                else
                {
                        this.Settings = bot.settings.misc[this.name];
                }
        }

        /**
         * Called when initializing the Bot,
         * when the configurations and such must be loaded
         * @param {Bot} bot 
         */
        OnBotReadData(bot)
        {
                
        }

        /**
         * Initialization function
         * @param {Bot} bot 
         */
        OnBotStart(bot)
        {
        }

        /**
         * Called when a message is received
         * @param {Bot} bot 
         * @param {Message} message 
         */
        OnMessageReceived(bot, message)
        {
        }

        OnPresenceChange(oldPresence, newPresence)
        {
                
        }
        
        /**
         * Called when the bot is shutting down
         */
        OnBotShutdown(bot)
        {
        }
        
        /**
         * Called when the bot must save the data
         * should be written as an async function
         * @param {Bot} bot 
         */
        OnBotSaveData(bot)
        {
                this.log("Saving settings");
                bot.settings.misc[this.name] = this.Settings;
        }

        static LoadModules(path)
        {
                let moduleArray = [];
                let files = Utils.ReadFilenamesFromDirectory(path);
                console.log("Loading modules from " + path)
                for(let i in files)
                {
                        let file = files[i];
                        if(file.endsWith(".js"))
                        {
                        let modulePath = Path.join(process.cwd(), path, file);
                        const loadedModule = new (require(modulePath)).Module;
                        console.log("Loaded module %s", loadedModule.name);
                        moduleArray.push(loadedModule);
                        }
                }

                console.log("Done loading modules!")
                return moduleArray;

        }

        log(message)
        {
                console.log("[ " + this.name + " ] : " + message);
        }

}

exports.IModule = IModule;
