const Utils = require ('./Utils.js').Utils;

class Settings {

    constructor(filePath)
        {
                if (typeof filePath !== 'undefined')
                {

                        let settingsString = Utils.ReadFileIfExists(filePath);
                        if(settingsString !== "")
                        {
                                let data = JSON.parse(settingsString);
                                //For every key in data, creates a new one into 
                                //the Settings instance and assings the read
                                //value to it
                                for(let thing in data)
                                {
                                        this[thing] = data[thing];
                                }
                        }
                        else
                        {
                                //Create a basic Settings instance which
                                //the user must edit, then closes the bot
                                this['bot_token'] = "";
                                this['bot_secret'] = "";
                                this['bot_add_token'] = "";
                                this['bot_master_admin'] = "";
                                this['bot_command_character'] = "~";
                                this['misc'] = {};
                                
                                Utils.SaveObjectToFile(filePath, this);
                                console.log("Created a new Settings file located in \"" + filePath + "\", edit it before \nrestarting the bot");
                                process.exit(0);
                        }
                        this.path = filePath;

                }
        }

        SaveDataToDisk()
        {
                let pathBackup = this.path;
                this.path = undefined;
                Utils.SaveObjectToFileAsync(pathBackup, this, (err) => {
                        if(err)
                                console.error("Cannot save bot settings! " + err);
                        
                        else
                        {
                                ///TODO: WTF IS THIS SHIT use a formatted date parser like %Y/%d/%M %h:%m:%s
                                let date = new Date();
                                console.log("[ Bot " + date.getFullYear() + "/" + date.getDay() + "/" + date.getMonth() + " " 
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + 
                                " ] : Settings saved succesfully!");
                        }
                });

                this.path = pathBackup;
                
        }

}

exports.Settings = Settings;
