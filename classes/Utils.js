const fs = require('fs');
const stringDecoder = require('string_decoder').StringDecoder;

/**
 * Checks if a string contains a certain substring
 */
String.prototype.contains = function(value)
{
        return this.indexOf(value) != -1;
}

class Utils
{
        /**
         * Reads the contents of a file if it exists, returns an empty string and 
         * creats the file otherwise
         * @param {String} filePath the file to read
         */
        static ReadFileIfExists(filePath)
        {
               
                if(fs.existsSync(filePath))
                {
                        //Reads data and decodes it
                        let decoder = new stringDecoder('utf8');
                        let data = fs.readFileSync(filePath);
                        
                        return (decoder.write(data));
                }
                else
                {
                        //Write empty string to create file
                        fs.writeFileSync(filePath, "", 'utf8');
                        return "";
                }
        }

        /**
         * Stringifies an object and writes its content to a file
         * @param {String} file The writing target
         * @param {*} object The object to stringify
         */
        static SaveObjectToFile(file, object)
        {
                let stringed = JSON.stringify(object);
                fs.writeFileSync(file, stringed, 'utf8');
        }

        /**
         * Stringifies an object and writes its content to a file, async way
         * @param {String} file The writing target
         * @param {*} object The object to stringify
         * @param {Function} callback
         */
        static SaveObjectToFileAsync(file, object, callback)
        {
                let stringed = JSON.stringify(object, null, 4);
                fs.writeFile(file, stringed, 'utf8', callback);
        }

        /**
         * Read filenames from a directory
         * @param {String} path 
         */
        static ReadFilenamesFromDirectory(path)
        {
                let files = fs.readdirSync(path, 'utf8');
                return files;
        }

        /**
         * Checks if a string is a mention
         * @param {String} name 
         */
        static IsAMention(name)
        {
                return (name.startsWith("<@") && name.endsWith(">"));
        }

        /**
         * Returns a string that will mention the user
         * @param {Number} id User ID
         */
        static GetMentionString(id)
        {
                return "<@" + id + ">";
        }

        /**
         * Removes the mention stuff from a string
         * @param {String} name 
         */
        static RemoveMention(name)
        {
                if(name.startsWith('<@') && name.endsWith(">"))
                {
                        return name.substring(2,name.length - 1);      
                }
                return name;
        }
}

exports.Utils = Utils;
