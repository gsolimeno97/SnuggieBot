const IModule = require('../classes/Module.js').IModule;

class UndiePlz extends IModule
{

    constructor()
    {
        super("UndiePlz","")
        this.isMiddyOn = true;
        this.isReady = false;
    }

    OnBotReadData(bot)
    {
        bot.fetchUser('186310820794204160')
        .then((result) => {
            this.isReady = true;
            this.isMiddyOn = result.presence.status === 'online';

            this.log("UndiePlz ready: Special user is " + result.presence.status);
        })
        .catch(() => {this.Log("Error fetching special user!")})
    }

    OnMessageReceived(bot, message)
    {
        //Bot is set to answer only to a specifi user dying
        ///TODO: Use a regex to see if the message was sent in an RP context
        ///TODO: add more replies
        if(this.isReady)
        {
            if(message.content.indexOf('dies') != -1 &&
                (message.author.id === '176147051178885120'))
            {
                if(!this.isMiddyOn)
                {
                    message.channel.send('Nuuuuu.\nUndie.')
                    .then(() => {this.log("Honk ded and livs again")})
                    .catch(console.error);
                }
                else
                {
                    this.log("Wont't revive Bane: Midnight is online")
                }
            }
        }
    }

    OnPresenceChange(oldPresence, newPresence)
    {
            if(newPresence.id == '186310820794204160')
            {
                if(newPresence.presence.status === 'online')
                    {
                        this.log("Mid is now online")
                        this.isMiddyOn = true;
                    }
                else  
                    {
                        this.log("Mid is offline");
                        this.isMiddyOn = false;
                    }
                }
    }

}

exports.Module = UndiePlz;