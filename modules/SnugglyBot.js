const IModule = require('../classes/Module.js').IModule;
const Util = require('util');
const Utils = require('../classes/Utils.js').Utils;

class SnugglyBot extends IModule
{

    constructor()
    {
        super("SnugglyBot","")
    }

    OnBotReadData(bot)
    {
        this.Phrases = this.Settings.snuggle_phrases;
        if(typeof this.Phrases === 'undefined')
        {
            this.Settings.snuggle_phrases = this.Phrases = 
            [
                "*Snugs %s*"
            ]
        }

        this.Settings.counter = 0;

        this.log("Initializing SnugglyBot Module")
        this.log("Phrases: \n " + this.Phrases);

        this.log("Settings loaded correctly!");
    }

    OnBotSaveData(bot)
    {

        this.Settings.counter ++;
        super.OnBotSaveData(bot); 
    }

    OnMessageReceived(bot, message)
    {

        //TODO: Improve algorithm
        if((message.content.toLowerCase().contains("snugs")&& message.content.toLowerCase().contains("require")) ||
            (message.content.toLowerCase().contains("snug")&& message.content.toLowerCase().contains("me")) ||
            (message.content.toLowerCase().contains("snugs") && message.content.toLowerCase().contains("want")))
        {
            let index = Math.floor(Math.random() * this.Phrases.length);
            let phrase = Util.format(this.Phrases[index], Utils.GetMentionString(message.author.id));
            message.channel.send(phrase)
                .then(() => {this.log("Cute message sent: " + phrase)})
                .catch(console.error);
        }
        ///TODO: Need to Regex this
        else if(message.content.contains("snug") && 
                message.content.length >= 2)
        {
            if(Utils.IsAMention(message.content.split(' ')[1]))
            {
            let snugTarget = message.content.split(' ')[1];
            if(Utils.RemoveMention(snugTarget) == bot.user.id)
            {
                message.channel.send("*Snugs herself*")
                .then(()=>{this.log("User wanted me to snug myself >c<")})
                .catch(console.error);
            }
            else
            {
                let index = Math.floor(Math.random() * this.Phrases.length);
                let phrase = Util.format(this.Phrases[index], snugTarget);
                message.channel.send(phrase)
                    .then(() => {this.log("Cute message sent: " + phrase)})
                    .catch(console.error);
            }
        }
        }
    }
}

exports.Module = SnugglyBot